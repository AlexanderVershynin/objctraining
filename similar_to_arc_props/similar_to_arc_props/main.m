//
//  main.m
//  similar_to_arc_props
//
//  Created by Александр on 1/12/18.
//  Copyright © 2018 Александр. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <objc/runtime.h>

@class CustomAutoreleasePool;

static CustomAutoreleasePool * g_pool = nil;



@interface CustomAutoreleasePool: NSObject
{
	IMP m_origAutorelease;
	NSPointerArray * m_pooledObjects;
	NSObject * m_previousPool;
}
@end

static void replacedAutorelease(id self, SEL releaseSelector, ...) {
	if (g_pool != nil)
	{
		[g_pool replacedAutolelease: self];
	}
}

@implementation CustomAutoreleasePool
-(void) replacedAutolelease: (NSObject*) obj
{
	[m_pooledObjects addPointer:obj];
}


-(id) init
{
	if (self = [super init])
	{
		m_pooledObjects = [[NSPointerArray alloc] initWithOptions: NSPointerFunctionsOpaqueMemory];
		m_previousPool = g_pool;
		g_pool = self;
		SEL originalSelector = @selector(autorelease);
		
		Method originalMethod = class_getInstanceMethod([NSObject class], originalSelector);
		
		m_origAutorelease = class_replaceMethod([NSObject class], @selector(autorelease),
																						replacedAutorelease, method_getTypeEncoding(originalMethod));
	}
	return self;
}


-(void)dealloc {
	for(id p in m_pooledObjects)
	{
		[p release];
	}
	g_pool = m_previousPool;
	[super dealloc];
}

@end





@interface NSBaseObject: NSObject
{
	NSMapTable  * m_destroyObservers;
}

-(void) addDestroyObserver:(NSBaseObject *) obj withSelector: (SEL) objSel;
-(void) removeDestroyObserver:(NSBaseObject *) obj;



@end

@implementation NSBaseObject

-(id) init
{
	if (self = [super init])
	{
		m_destroyObservers = [NSMapTable mapTableWithKeyOptions:NSMapTableWeakMemory | NSMapTableObjectPointerPersonality
																							 valueOptions:NSMapTableWeakMemory];
	}
	return self;
}

-(void) addDestroyObserver:(NSBaseObject *) obj withSelector: (SEL) objSel
{
	
	[m_destroyObservers setObject: [NSValue valueWithPointer: objSel] forKey: obj];
}

-(void) removeDestroyObserver:(NSBaseObject *) obj
{
	[m_destroyObservers removeObjectForKey:obj];
}

+(SEL)getterForPropertyWithName:(NSString*)name {
	const char* propertyName = [name cStringUsingEncoding:NSASCIIStringEncoding];
	objc_property_t prop = class_getProperty(self, propertyName);
	
	const char *selectorName = property_copyAttributeValue(prop, "G");
	if (selectorName == NULL) {
		selectorName = [name cStringUsingEncoding:NSASCIIStringEncoding];
	}
	NSString* selectorString = [NSString stringWithCString:selectorName encoding:NSASCIIStringEncoding];
	return NSSelectorFromString(selectorString);
}

+(SEL)setterForPropertyWithName:(NSString*)name {
	const char* propertyName = [name cStringUsingEncoding:NSASCIIStringEncoding];
	objc_property_t prop = class_getProperty(self, propertyName);
	
	char *selectorName = property_copyAttributeValue(prop, "S");
	NSString* selectorString;
	if (selectorName == NULL) {
		char firstChar = (char)toupper(propertyName[0]);
		NSString* capitalLetter = [NSString stringWithFormat:@"%c", firstChar];
		NSString* reminder      = [NSString stringWithCString: propertyName+1
																								 encoding: NSASCIIStringEncoding];
		selectorString = [@[@"set", capitalLetter, reminder, @":"] componentsJoinedByString:@""];
	} else {
		selectorString = [NSString stringWithCString:selectorName encoding:NSASCIIStringEncoding];
	}
	
	return NSSelectorFromString(selectorString);
}

-(void)dealloc {
	for(id key in m_destroyObservers)
	{
		SEL sel = [[m_destroyObservers objectForKey:key] pointerValue];
		[key performSelector:sel withObject:nil];
	}
	[super dealloc];
}

@end

@interface MyObject: NSBaseObject

@property NSBaseObject* someWeakProperty;
@property NSBaseObject* someStrongProperty;

@end

@implementation MyObject
@synthesize someWeakProperty = _someWeakProperty;
@synthesize someStrongProperty = _someStrongProperty;

-(void) setSomeWeakProperty:(NSBaseObject *)someWeakProperty
{
	_someWeakProperty = someWeakProperty;
	if (someWeakProperty != nil)
	{
		SEL sel = [NSBaseObject setterForPropertyWithName: @"someWeakProperty"];
		[_someWeakProperty addDestroyObserver: self withSelector: sel];
	}
}

-(void) setSomeStrongProperty:(NSBaseObject *)someStrongProperty
{
	[_someStrongProperty release];
	_someStrongProperty = someStrongProperty;
	[_someStrongProperty retain];
}

-(NSBaseObject*) someStrongProperty
{
	[_someStrongProperty release];
	return _someStrongProperty;
}

@end

int main(int argc, const char * argv[]) {
	{
		CustomAutoreleasePool * pool = [[CustomAutoreleasePool alloc] init];
	    // insert code here...
		NSLog(@"Hello, World!");
		MyObject * objContainer = [[MyObject alloc] init];
		NSBaseObject * obj1 = [[NSBaseObject alloc] init];
		NSBaseObject * obj2 = [[NSBaseObject alloc] init];
		[objContainer setSomeStrongProperty: obj1];
		[objContainer setSomeWeakProperty: obj2];
		[obj2 release];
		assert([objContainer someWeakProperty] == nil);
		[pool release];
	}
	return 0;
}



